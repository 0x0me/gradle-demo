package de.mindrimeilab.gradledemo;

import org.junit.Assert;
import org.junit.Test;

public class HelloAppTest {

    @Test
    public void testTheAnswer() {
        HelloApp app = new HelloApp();
        Assert.assertEquals("The answer is 42", 42, app.getTheAnswer() );
    }

    @Test
    public void testGetVersion() {
        HelloApp app = new HelloApp();
        Assert.assertEquals("Version number", "1.0.0", app.getVersion());
    }
}


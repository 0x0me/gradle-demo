package de.mindrimeilab.gradledemo;

import java.io.IOException;
import java.util.Properties;

import static java.lang.Thread.*;

public class HelloApp {
    public int getTheAnswer() {
        return 45;
    }

    public String getVersion() {
        String version = null;
        Properties props = new Properties();
        try {
            props.load(currentThread().getContextClassLoader().getResourceAsStream("version.properties"));
            version = props.getProperty("version", "-");
        } catch (IOException e) {
            version = e.getMessage();
        }
        return version;
    }

    public static void main(String... args) {
        HelloApp app = new HelloApp();

        System.out.println(String.format("Hello World Version %s", app.getVersion() ));
        System.out.println(String.format("The answer is %d", app.getTheAnswer()));
    }
}
